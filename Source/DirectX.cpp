#include "stdafx.h"
#include "DirectX.h"

CDirectX::CDirectX() :
m_hRenderTarget(NULL),
m_bInitialized(false),
m_iMSAACount(1),
m_iMSAAQuality(0),
m_BackBufferFormat(DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_UNORM),
m_pD3D11Device(nullptr),
m_pD3D11DeviceContext(nullptr),
m_pDXGISwapChain(nullptr),
m_pD3D11RenderTargetView(nullptr),
m_pD3D11DepthStencilView(nullptr),
m_pD3D11VertexBuffer(nullptr),
m_pD3D11IndexBuffer(nullptr)
{
	return;
}

CDirectX::~CDirectX()
{
	SafeCOMRelease(m_pD3D11DepthStencilView);
	SafeCOMRelease(m_pD3D11RenderTargetView);
	SafeCOMRelease(m_pDXGISwapChain);
	SafeCOMRelease(m_pD3D11DeviceContext);
	SafeCOMRelease(m_pD3D11Device);

	return;
}

bool CDirectX::Init(HWND hRenderTarget, UINT iRenderTargetWidth, UINT iRenderTargetHeight, ID3D11Device ** ppD3D11Device, ID3D11DeviceContext ** pD3D11DeviceContext)
{
	if (!m_bInitialized && hRenderTarget != NULL)
	{
		m_hRenderTarget = hRenderTarget;
		m_iBackBufferWidth = iRenderTargetWidth;
		m_iBackBufferHeight = iRenderTargetHeight;

		if (!CreateD3D11Device())
			return false;

		if (!CheckMSAASettings())
			return false;

		if (!CreateSwapChain())
			return false;

		if (!CreateRenderTarget())
			return false;

		if (!CreateDepthStencil())
			return false;

		m_pD3D11DeviceContext->OMSetRenderTargets(1, &m_pD3D11RenderTargetView, m_pD3D11DepthStencilView);

		D3D11_VIEWPORT vp; ZeroMemory(&vp, sizeof(D3D11_VIEWPORT));
		vp.Width = m_iBackBufferWidth;
		vp.Height = m_iBackBufferHeight;
		vp.MaxDepth = 1000.0f;;
		vp.MinDepth = 0.1f;
		vp.TopLeftX = 0;
		vp.TopLeftY = 0;

		m_pD3D11DeviceContext->RSSetViewports(1, &vp);

		*ppD3D11Device = m_pD3D11Device;
		*pD3D11DeviceContext = m_pD3D11DeviceContext;

		m_bInitialized = true;
		return true;
	}

	return false;
}

void CDirectX::PreDraw()
{
	static int sign = 1;
	static float rgba[4] = { 0, 0, 1, 1 };

	rgba[0] += sign * 0.0001f;

	if (rgba[0] >= 1 || rgba[0] <= 0)
		sign = -sign;

	m_pD3D11DeviceContext->ClearRenderTargetView(m_pD3D11RenderTargetView, rgba);

	return;
}

void CDirectX::Draw(UINT iIndexCount)
{
	if (m_pD3D11VertexBuffer != nullptr && m_pD3D11IndexBuffer != nullptr)
	{
		m_pD3D11DeviceContext->DrawIndexed(iIndexCount, 0, 0);
	}

	return;
}

void CDirectX::PostDraw()
{
	m_pDXGISwapChain->Present(0, 0);

	return;
}

void CDirectX::Resize(UINT iRenderTargetWidth, UINT iRenderTargetHeight)
{
	if (iRenderTargetWidth < 800 || iRenderTargetHeight < 600)
		throw;

	m_iBackBufferWidth = iRenderTargetWidth;
	m_iBackBufferHeight = iRenderTargetHeight;

	SafeCOMRelease(m_pD3D11RenderTargetView);

	HRESULT hr = S_OK;
	hr = m_pDXGISwapChain->ResizeBuffers(0, m_iBackBufferWidth, m_iBackBufferHeight, m_BackBufferFormat, 0);

	if (FAILED(hr))
		throw;

	if (!CreateRenderTarget())
		throw;

	SafeCOMRelease(m_pD3D11DepthStencilView);

	if (!CreateDepthStencil())
		throw;

	m_pD3D11DeviceContext->OMSetRenderTargets(1, &m_pD3D11RenderTargetView, m_pD3D11DepthStencilView);

	D3D11_VIEWPORT vp; ZeroMemory(&vp, sizeof(D3D11_VIEWPORT));
	vp.Width = m_iBackBufferWidth;
	vp.Height = m_iBackBufferHeight;
	vp.MaxDepth = 1000.0f;;
	vp.MinDepth = 0.1f;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;

	m_pD3D11DeviceContext->RSSetViewports(1, &vp);

	return;
}

inline bool CDirectX::CreateInputElemDesc(D3D11_INPUT_ELEMENT_DESC * pElemDesc, UINT iNumElems, void* pVSBlob, size_t szShaderSize, ID3D11InputLayout ** ppLayout)
{
	HRESULT hr = S_OK;
	hr = m_pD3D11Device->CreateInputLayout(pElemDesc, iNumElems, pVSBlob, szShaderSize, ppLayout);
	if (FAILED(hr))
		return false;

	return false;
}

bool CDirectX::CreateD3D11Device()
{
	if (m_pD3D11Device == nullptr && m_pD3D11DeviceContext == nullptr)
	{
		HRESULT hr = S_OK;
		hr = D3D11CreateDevice(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, D3D11_CREATE_DEVICE_DEBUG | D3D11_CREATE_DEVICE_SINGLETHREADED, nullptr, 0, D3D11_SDK_VERSION, &m_pD3D11Device, NULL, &m_pD3D11DeviceContext);

		if (FAILED(hr))
			return false;

		return true;
	}

	return false;
}

inline bool CDirectX::CheckMSAASettings()
{
	if (m_pD3D11Device != nullptr)
	{
		if (m_iMSAACount < 1)
			m_iMSAACount = 1;

		HRESULT hr = S_OK;
		hr = m_pD3D11Device->CheckMultisampleQualityLevels(m_BackBufferFormat, m_iMSAACount, &m_iMSAAQuality);

		if (FAILED(hr))
			return false;

		return true;
	}

	return false;
}

inline bool CDirectX::CreateSwapChain()
{
	if (m_pDXGISwapChain == nullptr && m_hRenderTarget != NULL && m_iBackBufferWidth > 800 && m_iBackBufferHeight > 600)
	{
		HRESULT hr = S_OK;
		IDXGIDevice * pDXGIDevice = nullptr;
		hr = m_pD3D11Device->QueryInterface(__uuidof(IDXGIDevice), (void**)&pDXGIDevice);

		if (FAILED(hr))
		{
			SafeCOMRelease(pDXGIDevice);
			return false;
		}

		IDXGIAdapter * pDXGIAdapter = nullptr;
		hr = pDXGIDevice->GetAdapter(&pDXGIAdapter);

		SafeCOMRelease(pDXGIDevice);

		if (FAILED(hr))
		{
			SafeCOMRelease(pDXGIAdapter);
			return false;
		}

		IDXGIFactory * pDXGIFactory = nullptr;

		hr = pDXGIAdapter->GetParent(__uuidof(IDXGIFactory), (void**)&pDXGIFactory);

		SafeCOMRelease(pDXGIAdapter);

		if (FAILED(hr))
		{
			SafeCOMRelease(pDXGIFactory);
			return false;
		}

		DXGI_SWAP_CHAIN_DESC scd; ZeroMemory(&scd, sizeof(DXGI_SWAP_CHAIN_DESC));
		scd.BufferCount = 1;
		scd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		scd.BufferDesc.Format = m_BackBufferFormat;
		scd.BufferDesc.Width = m_iBackBufferWidth;
		scd.BufferDesc.Height = m_iBackBufferHeight;
		scd.BufferDesc.RefreshRate.Numerator = 60;
		scd.BufferDesc.RefreshRate.Denominator = 1;
		scd.OutputWindow = m_hRenderTarget;
		scd.SampleDesc.Count = m_iMSAACount;
		scd.SampleDesc.Quality = m_iMSAAQuality - 1;
		scd.Windowed = true;
		scd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

		hr = pDXGIFactory->CreateSwapChain(m_pD3D11Device, &scd, &m_pDXGISwapChain);

		SafeCOMRelease(pDXGIFactory);

		if (FAILED(hr))
			return false;

		return true;
	}

	return false;
}

bool CDirectX::CreateRenderTarget()
{
	if (m_pD3D11Device != nullptr && m_pDXGISwapChain != nullptr)
	{
		HRESULT hr = S_OK;

		ID3D11Buffer * pBackBuffer = nullptr;
		hr = m_pDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&pBackBuffer);

		if (FAILED(hr))
		{
			SafeCOMRelease(pBackBuffer);
			return false;
		}

		hr = m_pD3D11Device->CreateRenderTargetView(pBackBuffer, NULL, &m_pD3D11RenderTargetView);

		SafeCOMRelease(pBackBuffer);

		if (FAILED(hr))
			return false;
		

		return true;
	}

	return false;
}

inline bool CDirectX::CreateDepthStencil()
{
	if (m_pD3D11Device != nullptr)
	{
		HRESULT hr = S_OK;
		D3D11_TEXTURE2D_DESC dsd; ZeroMemory(&dsd, sizeof(D3D11_TEXTURE2D_DESC));
		dsd.Width = m_iBackBufferWidth;
		dsd.Height = m_iBackBufferHeight;
		dsd.SampleDesc.Count = m_iMSAACount;
		dsd.SampleDesc.Quality = m_iMSAAQuality - 1;
		dsd.MipLevels = 1;
		dsd.ArraySize = 1;
		dsd.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
		dsd.Usage = D3D11_USAGE_DEFAULT;
		dsd.BindFlags = D3D11_BIND_DEPTH_STENCIL;

		ID3D11Texture2D * pText2d = nullptr;
		hr = m_pD3D11Device->CreateTexture2D(&dsd, nullptr, &pText2d);
		if (FAILED(hr))
			return false;

		hr = m_pD3D11Device->CreateDepthStencilView(pText2d, 0, &m_pD3D11DepthStencilView);
		if (FAILED(hr))
			return false;

		SafeCOMRelease(pText2d);

		return true;
	}

	return false;
}