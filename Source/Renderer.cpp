#include "stdafx.h"
#include "Renderer.h"

CRenderer::CRenderer():
m_bInitialized(false),
m_pD3D11Device(nullptr),
m_pD3D11DeviceContext(nullptr),
m_pDirectX(nullptr)
{
	return;
}

CRenderer::~CRenderer()
{
	delete m_pDirectX;
	m_pDirectX = nullptr;

	m_bInitialized = false;

	return;
}

bool CRenderer::Init(HWND hRenderTarget, UINT iRenderTargetWidth, UINT iRenderTargetHeight)
{
	if (!m_bInitialized && hRenderTarget != NULL)
	{
		m_hRenderTarget = hRenderTarget;

		if (m_pDirectX != nullptr)
			return false;

		m_pDirectX = new CDirectX();
		if (!m_pDirectX->Init(m_hRenderTarget, iRenderTargetWidth, iRenderTargetHeight, &m_pD3D11Device, &m_pD3D11DeviceContext))
			return false;

		m_bInitialized = true;
		return true;
	}

	return false;
}

void CRenderer::PreDraw()
{
	m_pDirectX->PreDraw();

	return;
}

void CRenderer::Draw()
{
	return;
}

void CRenderer::PostDraw()
{
	m_pDirectX->PostDraw();

	return;
}

void CRenderer::Resize(UINT iRenderTargetWidth, UINT iRenderTargetHeight)
{
	if (m_pDirectX != nullptr)
		m_pDirectX->Resize(iRenderTargetWidth, iRenderTargetHeight);

	return;
}