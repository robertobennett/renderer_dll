#pragma once
#ifndef RENDERER_DLL_I_SHADER_H
#define RENDERER_DLL_I_SHADER_H

#include <d3d11.h>
#include <dxgi.h>

class IShaderComponent
{
	public:
		IShaderComponent();
		virtual ~IShaderComponent();

	private:

};

#endif