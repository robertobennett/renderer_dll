#pragma once
#ifndef RENDERER_DLL_DIRECT_X_H
#define RENDERER_DLL_DIRECT_X_H

#include <d3d11.h>
#include <dxgi.h>

class CDirectX
{
	public:
		CDirectX();
		~CDirectX();

		bool Init(HWND hRenderTarget, UINT iRenderTargetWidth, UINT iRenderTargetHeight, ID3D11Device ** ppD3D11Device, ID3D11DeviceContext ** pD3D11DeviceContext);
		void PreDraw();
		void Draw(UINT iIndexCount);
		void PostDraw();
		void Resize(UINT iRenderTargetWidth, UINT iRenderTargetHeight);

		inline bool CreateInputElemDesc(D3D11_INPUT_ELEMENT_DESC * pElemDesc, UINT iNumElems, void* pVSBlob, size_t szShaderSize, ID3D11InputLayout ** ppLayout);

	private:
		HWND m_hRenderTarget;
		bool m_bInitialized;
		UINT m_iMSAACount;
		UINT m_iMSAAQuality;
		UINT m_iBackBufferWidth;
		UINT m_iBackBufferHeight;

		DXGI_FORMAT m_BackBufferFormat;
		ID3D11Device * m_pD3D11Device;
		ID3D11DeviceContext * m_pD3D11DeviceContext;
		IDXGISwapChain * m_pDXGISwapChain;
		ID3D11RenderTargetView * m_pD3D11RenderTargetView;
		ID3D11DepthStencilView * m_pD3D11DepthStencilView;

		ID3D11Buffer * m_pD3D11VertexBuffer;
		ID3D11Buffer * m_pD3D11IndexBuffer;

		inline bool CreateD3D11Device();
		inline bool CheckMSAASettings();
		inline bool CreateSwapChain();
		inline bool CreateRenderTarget();
		inline bool CreateDepthStencil();
};

#endif