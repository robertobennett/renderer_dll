#pragma once
#ifndef RENDERER_DLL_RENDERER_H
#define RENDERER_DLL_RENDERER_H

#include "DirectX.h"

class CRenderer
{
	public:
		DLL_IMPORT_EXPORT CRenderer();
		DLL_IMPORT_EXPORT ~CRenderer();

		DLL_IMPORT_EXPORT bool Init(HWND hRenderTarget, UINT iRenderTargetWidth, UINT iRenderTargetHeight);
		DLL_IMPORT_EXPORT void PreDraw();
		DLL_IMPORT_EXPORT void Draw();
		DLL_IMPORT_EXPORT void PostDraw();
		DLL_IMPORT_EXPORT void Resize(UINT iRenderTargetWidth, UINT iRenderTargetHeight);

		

	private:
		HWND m_hRenderTarget;
		bool m_bInitialized;

		ID3D11Device * m_pD3D11Device;
		ID3D11DeviceContext * m_pD3D11DeviceContext;

		CDirectX * m_pDirectX;
};

#endif