#pragma once
#ifndef RENDERER_DLL_STDAFX_H
#define RENDERER_DLL_STDAFX_H

#include <vector>
#include <stdlib.h>
#include <string>
#include <sstream>
#include <fstream>

#include <Windows.h>

#include <d3d11.h>
#include <dxgi.h>
#include <DirectXMath.h>

#ifdef _WINDLL
#define DLL_IMPORT_EXPORT __declspec(dllexport)
#else
#define DLL_IMPORT_EXPORT __declspec(dllimport)
#endif

#define SafeCOMRelease(x) { if(x != nullptr) { x->Release(); x = nullptr; } }

#endif